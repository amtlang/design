# Goals of amtlang

- Fully capsulated actors
  - Protocols define API's
  - Protocols can contain observable events

- Actor management is easy
  - Application tree allows easy and global access
  - Common protocols for Supervisor's
  - Auto derive for common Supoervisor implementations?
  